function sortTable(n){
    var table,rows,switching,i,x,y,shouldSwitch,dir,switchcount = 0;
    table = document.getElementById("TheyGarraPay");
    switching=true;
    //set the sorting direction to ascending
    dir = "asc";
    /* make a loop that will continue until no switching was done: */
    while(switching){
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*loop through all the table rows(except the first ,which is contains the table headers):*/
        for (i=1;1<(rows.length-1);i++){
            //Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements that i want to compare,one from the current row and one from the next: */
            x = rows[i].getElementByTagName("td")[n];
            y = rows[i+1].getElementByTagName("td")[n];
            /* check if the two rows should switch place,based on the direction ,asc or desc: */
            if (dir == "asc"){
                if(x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()){
                    //if so ,mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    //if so ,mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if(shouldSwitch){
            /* if a switch has marked ,make the swtich and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i+1],rows[i]);
            switching = true;
            //Each time a switch is done,increase this count by 1:
            switchcount++;
        }else{
            /*if no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again .*/
        if(switchcount == 0 && dir == "asc"){
            dir = "desc";
            switching = true;
        }   
        }
    }

}