from django.conf.urls import url
from twinkle import views

from . import views

urlpatterns = [
    url(r'^$',views.index,name='index'),
    url(r'^register/$',views.register,name='register'),
    url(r'^about/$',views.about,name='about'),
    url(r'^parents/$',views.view_parent,name='view_parents'),
    url(r'^children/$',views.view_children,name='view_children'),
    url(r'^update_children/',views.update_children,name='update_children'),
    url(r'^delete_child/',views.delete_child,name='delete_child'),
    url(r'^addpayments/',views.view_payments,name='view_payments'),
    url(r'^payments/$',views.add_payments,name='add_payments'),
    url(r'^defaulters/$',views.defaulters,name='defaulters'),
    url(r'^update_parents/',views.update_parents,name='update_parents'),
    url(r'^delete_parent/',views.delete_parent,name='delete_parent'),
    url(r'^requirements_defaulters/$',views.requirements_defaulters,name='requirements_defaulters'),
    url(r'^requirements/$',views.add_requirements,name='add_requirements'),
    url(r'^show_payment_history/$',views.show_payment_history,name='show_payment_history'),
    url(r'^update_show_payment_history/',views.update_show_payment_history,name='update_show_payment_history'),
    url(r'^delete_show_payment_history/',views.delete_show_payment_history,name='delete_show_payment_history'),
    url(r'^user_register/',views.user_register,name='user_register'),
    url(r'^user_login/$', views.user_login, name='user_login'),
    url(r'^user_logout/$', views.user_logout, name='user_logout'),
]