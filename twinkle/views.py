from django.shortcuts import render
from django.http import HttpResponse
from twinkle.models import parents,children,payments,requirements
from twinkle.forms import parentForm , childrenForm ,payments_form,requirements_form
from django.views.generic import TemplateView, ListView
from django.db.models import Q

from collections import Counter
from django.shortcuts import (get_object_or_404,render,HttpResponseRedirect,redirect)
from django.forms import ModelForm
from twinkle.forms import UserForm
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.contrib.auth import authenticate, login
class parent_form(parentForm):
    class Meta:
        model = parents
        exclude = ("id",)


def index(request):
    context_dict = {'boldmessage':"We grow ,We laugh ,We learn"}

    return render(request,'twinkle/index.html',context=context_dict)

def about(request):
    context_dict = {'ceo':"CHIEF EXECUTIVE OFFICER - KYOMUHENDO BETTY CONTACT 0787006984"}

    return render(request,'twinkle/about.html',context=context_dict)

def view_parent(request):

    parent_list = parents.objects.order_by('-father_firstname')
    context_dict = {'parents' : parent_list}

    return render(request,'twinkle/parents.html',context_dict)
    
def update_parents(request):
    # parent = get_object_or_404(parents,id=id)
    #pk = request.GET['id']
    context_dict = {}
    if 'id' in request.GET:
        # cleaned_pk = int(clean_pk)
        pk = request.GET['id']
        print (pk)
        clean_pk = pk.strip("/")
        print (clean_pk)
        parent = parents.objects.get(id=clean_pk)

        form = parent_form(request.POST or None, instance=parent)

        if form.is_valid():
            form.save()
            redirect('view_parents')

        context_dict["form"] = form
    return render(request,"twinkle/update_parents.html",context=context_dict)

def delete_parent(request):
    # book= get_object_or_404(Book, pk=pk)  
    context_dict = {}

    if 'id' in request.GET:
        pk = request.GET['id']
        clean_pk = pk.strip("/")
        cleaned_pk = int(clean_pk)
        parent = parents.objects.get(id=cleaned_pk)  
        if request.method=='POST':
            parent.delete()
            return redirect('view_parents')
        context_dict["object"] = parent
    return render(request, "twinkle/delete_parent.html",context=context_dict)

def view_children(request):
    child_list = children.objects.order_by('-firstname')
    context_dict = {'children':child_list}

    return render(request,'twinkle/children.html',context_dict)

def update_children(request):
    context_dict = {}

    if 'id' in request.GET:
        pk = request.GET['id']
        print (pk)
        clean_pk = pk.strip("/")
        print (clean_pk)
        child = children.objects.get(id=int(clean_pk))
        form = childrenForm(request.POST or None, instance=child)

        if form.is_valid():
            form.save()
            redirect('view_children')

        context_dict["form"] = form

    return render(request,"twinkle/update_children.html",context=context_dict)

def delete_child(request):

    context_dict = {}

    if 'id' in request.GET:
        pk = request.GET['id']
        clean_pk = pk.strip("/")
        print (clean_pk)
        child = children.objects.get(id=int(clean_pk))  
        if request.method=='POST':
            child.delete()
            return redirect('view_children')
        context_dict["object"] = child

    return render(request, "twinkle/delete_child.html",context=context_dict )

def register(request):
  
    registered = False
    
    if request.method == 'POST':
        parent_form = parentForm(data=request.POST)

        children_form = childrenForm(data=request.POST)
       
        if parent_form.is_valid() and children_form.is_valid():

            parent = parent_form.save()

            parent.save()

            children = children_form.save()

            children.parent = parent

            children.save()

            registered = True
            print("Registration successful")
        else:
            print(parent_form.errors, children_form.errors)
    else:
        parent_form = parentForm()

        children_form = childrenForm()

    return render(request,'twinkle/register.html',{'parent_form':parent_form,'children_form':children_form,registered:'registered'})

def view_payments(request):
    if request.user.is_authenticated() == False:
        context_dict = {'message':" Hello Guest you are not logged in"}

        return render(request,'twinkle/not_loggedin.html',context=context_dict)
        
    else:
        query = request.GET.get('child')
        if query != None:
            clean_query = query.rstrip()
            kid = children.objects.filter(firstname=clean_query)
            return render(request,'twinkle/addpayments.html',{'kid':kid})
        else:
            return render(request,'twinkle/addpayments.html',{})
        
def show_payment_history(request):
    child = request.GET.get('child')
    start_date = request.GET.get('StartDate')
    end_date = request.GET.get('EndDate')
    # payment = payments.objects.filter(child=query)
    # payment = payments.objects.filter(Q(date__range=[start_date, end_date]),child=child,)
    payment = payments.objects.filter(child=child)
    #payment = payments.objects.filter(Q(date=start_date) | Q(date=end_date),child=child,)
    kid = children.objects.filter(id=child)
    if payment:
        available = True
    else:
        available = False
    return render(request,'twinkle/show_payment_history.html',{'payment':payment,'available':available,'kid':kid})

def update_show_payment_history(request):
    context_dict = {}

    if 'id' in request.GET:
        pk = request.GET['id']
    
        print (pk)
        clean_pk = pk.strip("/")
        print (clean_pk)
        payment = payments.objects.get(id=clean_pk)
        form = payments_form(request.POST or None, instance=payment)
    
        if form.is_valid():
            form.save()
            redirect('show_payment_history')

        context_dict["form"] = form
    return render(request,"twinkle/update_show_payment_history.html",context=context_dict)

def delete_show_payment_history(request):
    
    context_dict = {}

    if 'id' in request.GET:
        pk = request.GET['id']
        clean_pk = pk.strip("/")
        print (clean_pk)
        payment = payments.objects.get(id=int(clean_pk)) 

        if request.method=='POST':
            payment.delete()
            return redirect('show_payment_history')

        context_dict["object"] = payment
        
    return render(request, "twinkle/delete_show_payment_history.html",context=context_dict)

def defaulters(request):
    start_date = request.GET.get('StartDate')
    end_date = request.GET.get('EndDate')
    gender = request.GET.get('gender')
    classlevel = request.GET.get('classlevel')
    time_of_study = request.GET.get('time_of_study')
    campus = request.GET.get('campus')

    defaulters = payments.objects.filter(Q(date__range=[start_date, end_date]),)
    
    #Child array
    arr = []
    for x in defaulters:
        arr.append(x.child) 
    print(arr)

    #Amount array
    fees = []
    for x in defaulters:
        fees.append(x.amount)       
    print(fees)

    repeating_elements = []
    final_dict = {}
    unique_final_dict = {}
    unique_elements = []
    non_repeating_elements = []
    print("Repeating elements at positions ") 
    array_length = len(arr)
    amount_length = len(fees)
    non_repeating_length = len(non_repeating_elements)
    
    for i in range(0,array_length):
        for j in range (i+1,array_length):
            if arr[i] == arr[j]:
                get_indexes = lambda x, xs: [i for (y, i) in zip(xs, range(len(xs))) if x == y]
                print ("child id",arr[i],"is at positon")
                m = get_indexes(arr[i],arr)
                print (m)
               
                x = 0
                amount = 0
                # Iterating using while loop 
                while x < len(fees): 
                    #print(fees[x]) 
                    g = fees[x]
                    f = fees.index(fees[x]) 
                    x = x+1
                   
                    for v in m:
                        if v == f:
                            amount += g
                print ("Total amount is ",amount)

                if arr[i] not in repeating_elements:
                    repeating_elements.append(arr[i])
                print ("Repeating elements are ",repeating_elements)

                if arr[i] not in final_dict:
                    final_dict[arr[i]] = amount  
    print ("The consolidated dictionary of values is ",final_dict)
        
    #look for non repeating elements now
    print ("The unique elements are ")
    unique_set = set(arr)
    unique_elements = (list(unique_set)) 
    for k in unique_elements:
        if k not in repeating_elements:
            print (k)
            non_repeating_elements.append(k)
        print (non_repeating_elements)

        if k in arr:
            get_index = lambda x, xs: [i for (y, i) in zip(xs, range(len(xs))) if x == y]
            print ("Non repeating child id",k,"is at positon")
            b = get_index(k,arr)
            print (b)

            y = 0
            full_amount = 0
            # Iterating using while loop
            for y in range(0,len(fees)):
                c = fees[y]
                j = fees.index(fees[y])
                for n in b:
                    if n == j:
                        full_amount += c
            print ("Full amount is ",full_amount)

            if k not in final_dict:
                final_dict[k] = full_amount
            print ("The final dictionary of values is ",final_dict)
        print (final_dict)

    specifics = []
    count = 0
    for k ,value in final_dict.items():
        details = children.objects.values().filter(id=k)
        addition = details[0]
        addition["amount"] = value
        count+=1
        specifics.append(addition)
    print (specifics)
    return render(request,'twinkle/defaulters.html',{'defaulters':defaulters,'final_dict':final_dict,'specifics':specifics})

def requirements_defaulters(request):
    start_date = request.GET.get('StartDate')
    end_date = request.GET.get('EndDate')

    #we are getting rows of requirements made
    defaulters = requirements.objects.filter(Q(date__range=[start_date, end_date]),)
    
    #Child array
    #we put the child ids inside an array
    arr = []
    for x in defaulters:
        arr.append(x.child) 
    print(arr)

    #Amount array
    #we put the amounts of requirements inside an array
    fees = []
    for x in defaulters:
        fees.append(x.amount)       
    print(fees)

    repeating_elements = []
    final_dict = {}
    unique_final_dict = {}
    unique_elements = []
    non_repeating_elements = []
    print("Repeating elements at positions ") 
    #we are getting how many ids we managed to capture
    array_length = len(arr)
    #we are counting how manay amounts did we capture
    amount_length = len(fees)
    #we are counting elements that are unique
    non_repeating_length = len(non_repeating_elements)
    
    #for every child id inside that array
    for i in range(0,array_length):
        #move to the next child_id in that array
        for j in range (i+1,array_length):
            #if the next child_id is the same with the other
            if arr[i] == arr[j]:
                #get the positions of the non-unique child_id
                get_indexes = lambda x, xs: [i for (y, i) in zip(xs, range(len(xs))) if x == y]
                #print its position
                print ("child id",arr[i],"is at positon")
                m = get_indexes(arr[i],arr)
                print (m)
               
                x = 0
                amount = 0
                # Iterating using while loop 
                while x < len(fees): 
                    #print(fees[x]) 
                    #for every element in the amount array
                    g = fees[x]
                    #get its position
                    f = fees.index(fees[x]) 
                    x = x+1

                    #for every position of the non-unique child-id
                    for v in m:
                        #if it is the same as the position of the amount
                        if v == f:
                            #consolidate the amounts together
                            amount += g
                print ("Total amount is ",amount)
                #if the child-id is not unique
                if arr[i] not in repeating_elements:
                    #append it to the repeating_elements list
                    repeating_elements.append(arr[i])
                print ("Repeating elements are ",repeating_elements)

                #if the child is not in final_dict already add it inside 
                # with its key and value 
                if arr[i] not in final_dict:
                    final_dict[arr[i]] = amount  
    print ("The consolidated dictionary of values is ",final_dict)
        
    #look for non repeating elements now
    print ("The unique elements are ")
    #here we look for unique child-ids
    unique_set = set(arr)
    #put them inside a list
    unique_elements = (list(unique_set)) 
    #for every element inside the list
    for k in unique_elements:
        #if the element is not in repeating_elements
        if k not in repeating_elements:
            print (k)
            #put it inside the non_repeating elements
            non_repeating_elements.append(k)
        print (non_repeating_elements)

        #check to see which position is the element at
        if k in arr:
            get_index = lambda x, xs: [i for (y, i) in zip(xs, range(len(xs))) if x == y]
            print ("Non repeating child id",k,"is at positon")
            #get the position
            b = get_index(k,arr)
            print (b)

            y = 0
            full_amount = 0
            # Iterating using while loop
            for y in range(0,len(fees)):
                c = fees[y]
                j = fees.index(fees[y])
                for n in b:
                    if n == j:
                        full_amount += c
            print ("Full amount is ",full_amount)

            if k not in final_dict:
                final_dict[k] = full_amount
            print ("The final dictionary of values is ",final_dict)
        print (final_dict)

    exact_list = []
    count = 0
    for k ,value in final_dict.items():
        details = children.objects.values().filter(id=k)
        addition = details[0]
        addition["amount"] = value
        count+=1
        exact_list.append(addition)
    print (exact_list)
    
    return render(request,'twinkle/requirements_defaulters.html',{'exact_list':exact_list})

@login_required
def add_payments(request):
   
    if  request.user.is_authenticated():
        #return HttpResponse("You are logged in.")

        if request.method =='POST':
            payments_form_one = payments_form(data=request.POST)
            childid = request.POST.get("child")
            print (childid)
            query = payments.objects.filter(child=childid)
            if query:
                if payments_form_one.is_valid():
                    payment = payments_form_one.save()
                    
                    payment.save()
                else:
                    print(payments_form_one.errors)
            else:
                    print(payments_form_one.errors)
        else:
            payments_form_one = payments_form()

        return render(request,'twinkle/payments.html',{'payments_form_one':payments_form_one})
    else:
        return HttpResponse("You are not logged in.")

def add_requirements(request):
    if request.method == 'POST':
        requirements_form_one = requirements_form(data=request.POST)
        childid = request.POST.get("child")
        query = payments.objects.filter(child=childid)
        if query:
            if requirements_form_one.is_valid():
                requirements = requirements_form_one.save()
                requirements.save()
            else:
                print (requirements_form_one.errors)
        else:
            print (requirements_form_one.errors)
    else:
        requirements_form_one = requirements_form()

    return render(request,'twinkle/requirements.html',{'requirements_form':requirements_form})

def user_register(request):

    registered = False

    if request.method == 'POST':
        user_form = UserForm(data=request.POST)

        if user_form.is_valid():

            user = user_form.save()

            user.set_password(user.password)
            user.save()

            registered = True
        else:
            print(user_form.errors)
    else:
        user_form = UserForm()

    return render(request,'twinkle/user_register.html',{'user_form':user_form,'registered':registered})

def user_login(request):
 
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username,password=password)

        if user is not None:
            if user.is_active:
                login(request,user)
                print("Valid login details")
                return HttpResponseRedirect(reverse('index'))
                print("Valid login details")
            else:
                return HttpResponse("Your account has been disabled")
        else:
            # print("Invalid login details")
            print("Invalid login details: {0}, {1}".format(username, password))
    else:
        return render(request,'twinkle/user_login.html',{})

def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    if request.user.is_authenticated():
        logout(request)
    # Take the user back to the homepage.
    else:
        return HttpResponseRedirect(reverse('index'))
    return render(request,'twinkle/index.html',{})

