from django.test import SimpleTestCase
from twinkle.forms import *

class TestForms(SimpleTestCase):

    def test_parentForm_valid_data(self):
        form = parentForm(data={
            'father_firstname' :'nkaheebwah',
            'father_lastname' :'harrison',
            'father_phonenumber_one':'7',
            'father_phonenumber_two':'7',
            'mother_firstname':'kyomuhendo',
            'mother_lastname':'betty',
            'mother_phonenumber_one':'7',
            'mother_phonenumber_two':'7',
            'address':'hoima'
        })

        self.assertTrue(form.is_valid())

    def test_parent_form_no_data(self):
        form = parentForm(data={})

        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors),9)