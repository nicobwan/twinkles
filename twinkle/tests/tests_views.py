from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from twinkle.models import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
import json

class TestViews(TestCase):
    
    def setUp(self):
        self.client = Client()
        self.index_url = reverse('index')
        self.about_url = reverse('about')
        self.view_parent_url = reverse('view_parents')
        self.update_parents_url = reverse('update_parents')
        self.delete_parent_url = reverse('delete_parent')
        self.view_children_url = reverse('view_children')
        self.update_children_url = reverse('update_children')
        self.delete_child_url = reverse('delete_child')
        self.view_payments_url = reverse('view_payments')
        self.register_url = reverse('register')
        self.user_login_url = reverse('user_login')
        self.show_payment_history_url = reverse('show_payment_history')
        self.update_payment_history_url = reverse('update_show_payment_history')
        self.delete_show_payment_history_url =reverse('delete_show_payment_history')
        self.defaulters_url = reverse('defaulters')
        self.requirements_defaulters_url = reverse('requirements_defaulters')
        self.add_payments_url = reverse('add_payments')
        self.add_requirements_url = reverse('add_requirements')

    def test_index_GET(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/index.html')

    def test_about_GET(self):
        response = self.client.get(self.about_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/about.html')

    def test_update_parents_GET(self):
        response = self.client.get(self.update_parents_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/update_parents.html')

    def test_delete_parent_GET(self):
        response = self.client.get(self.delete_parent_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/delete_parent.html')

    def test_view_children_GET(self):
        response = self.client.get(self.view_children_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/children.html')

    def test_update_children_GET(self):
        response = self.client.get(self.update_children_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/update_children.html')

    def test_delete_child_GET(self):
        response = self.client.get(self.delete_child_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/delete_child.html')

    def test_register_POST(self):
        response = self.client.post(self.register_url,{ 
            'father_firstname' :'aheebwah',
            'father_lastname' : 'marvin',
            'father_phonenumber_one' : '7',
            'father_phonenumber_two' : '7',
            'mother_firstname' : 'leero',
            'mother_lastname' : 'jude',
            'mother_phonenumber_one' : '5',
            'mother_phonenumber_two' : '5',
            'address' : 'kampala',
            'classlevel':'BabyLion',
            'firstname':'tony',
            'middlename':'jr',
            'lastname':'moose',
            'age':'3',
            'time_of_study':'Halfday',
            'gender':'Male',
            'campus':'TownCampus'
            }  
            )
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/register.html')

    def test_user_login_POST(self):
        # c.force_login(self.user)
        # response = self.client.post(self.user_login_url)
        # response.status_code
        # self.assertEquals(response.status_code,200)
        # self.assertEquals(response.status_code,200)
        # self.assertTemplateUsed(response,'twinkle/user_login.html')
        self.user = User.objects.create(username='alice', password='Loading@1', is_active=True, is_staff=True, is_superuser=True) 
        self.user.set_password('Loading@1') 
        self.user.save() 
        self.user = authenticate(username='alice', password='Loading@1') 
        login = self.client.login(username='alice', password='Loading@1') 
        self.assertTrue(login)

        #since we have logged in ,i can now look at other various aspects inside payments
        response = self.client.get(self.view_payments_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/addpayments.html')

        #show the payment history 
        response = self.client.get(self.show_payment_history_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/show_payment_history.html')

        #update_show_payment_history
        response = self.client.get(self.update_payment_history_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/update_show_payment_history.html')

        #delete_show_payment_history
        response = self.client.get(self.delete_show_payment_history_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/delete_show_payment_history.html')

        #defaulters
        response = self.client.get(self.defaulters_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/defaulters.html')

        #requirements_defaulters
        response = self.client.get(self.requirements_defaulters_url)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/requirements_defaulters.html')

        #add_payments
        response = self.client.post(self.add_payments_url,{
            'child':'4',
            'amount':'70000',
            'date':'2021-04-12',
            'bank':'PostBank',
            'term':'1'
        })
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/payments.html')

        #add_requirements
        response = self.client.post(self.add_requirements_url,{
            'child':'4',
            'amount':'70000',
            'date':'2021-02-10',
            'bag':'1',
            'egg':'1'
        })
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,'twinkle/requirements.html')