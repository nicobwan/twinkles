from django.test import SimpleTestCase
from django.core.urlresolvers import reverse,resolve
from twinkle.views import *
class TestUrls(SimpleTestCase):

    def test_index_url_is_resolved(self):
        url = reverse('index')
        self.assertEquals(resolve(url).func,index)
    
    def test_about_url_is_resolved(self):
        url = reverse('about')
        self.assertEquals(resolve(url).func,about)

    def test_view_parent_url_is_resolved(self):
        url = reverse('view_parents')
        self.assertEquals(resolve(url).func,view_parent)

    def test_update_parents_is_resolved(self):
        url = reverse('update_parents')
        self.assertEquals(resolve(url).func,update_parents)

    def test_delete_parent_is_resolved(self):
        url = reverse('delete_parent')
        self.assertEquals(resolve(url).func,delete_parent)

    def test_view_children_is_resolved(self):
        url = reverse('view_children')
        self.assertEquals(resolve(url).func,view_children)

    def test_update_children_is_resolved(self):
        url = reverse('update_children')
        self.assertEquals(resolve(url).func,update_children)

    def test_delete_child_is_resolved(self):
        url = reverse('delete_child')
        self.assertEquals(resolve(url).func,delete_child) 

    def test_register_is_resolved(self):
        url = reverse('register')
        self.assertEquals(resolve(url).func,register)

    def test_view_payments_is_resolved(self):
        url = reverse('view_payments')
        self.assertEquals(resolve(url).func,view_payments)

    def test_show_payment_history_is_resolved(self):
        url = reverse('show_payment_history')
        self.assertEquals(resolve(url).func,show_payment_history)  

    def test_delete_payment_history_is_resolved(self):
        url = reverse('delete_show_payment_history')
        self.assertEquals(resolve(url).func,delete_show_payment_history)  

    def test_defaulters_is_resolved(self):
        url = reverse('defaulters')
        self.assertEquals(resolve(url).func,defaulters)  

    def test_requirements_defaulters_is_resolved(self):
        url = reverse('requirements_defaulters')
        self.assertEquals(resolve(url).func,requirements_defaulters)

    def test_add_payments_is_resolved(self):
        url = reverse('add_payments')
        self.assertEquals(resolve(url).func,add_payments)

    def test_add_requirements_is_resolved(self):
        url = reverse('add_requirements')
        self.assertEquals(resolve(url).func,add_requirements)

    def test_user_register(self):
        url = reverse('user_register')
        self.assertEquals(resolve(url).func,user_register)

    def test_user_login(self):
        url = reverse('user_login')
        self.assertEquals(resolve(url).func,user_login)

    def test_user_logout(self):
        url = reverse('user_logout')
        self.assertEquals(resolve(url).func,user_logout)


 
    
    