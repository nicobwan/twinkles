from django import forms
from django.utils import timezone
from datetime import date
from twinkle.models import parents,children,payments,requirements
from django.contrib.auth.models import User
class parentForm(forms.ModelForm):
    father_firstname = forms.CharField(max_length=100)
    father_lastname = forms.CharField(max_length=100)
    father_phonenumber_one = forms.IntegerField()
    father_phonenumber_two = forms.IntegerField()
    mother_firstname = forms.CharField(max_length=100)
    mother_lastname = forms.CharField(max_length=100)
    mother_phonenumber_one = forms.IntegerField()
    mother_phonenumber_two = forms.IntegerField()
    address = forms.CharField(max_length=100)

    class Meta:
        model = parents
        fields =('father_firstname','father_lastname','father_phonenumber_one','father_phonenumber_two','mother_firstname'
                    ,'mother_lastname','mother_phonenumber_one','mother_phonenumber_two','address')


classlevel_choices = (("BabyLion","BabyLion"),("BabyZebra","BabyZebra"),("Baby","Baby"),("Middle","Middle"),("Top","Top"),("P1","P1"),)
age_choices = (("2", "Two"), ("3", "Three"),("4","Four"),("5","Five"),("6","Six"),)
gender_choices = (("Male","Male"),("Female","Female"),)
time_of_study_choices = (("Halfday","HalfDay"),("Fullday","Fullday"),)
campus_choices = (("TownCampus","TownCampus"),("KasingoAnnex","KasingoAnnex"),)

class childrenForm(forms.ModelForm):
    #tutorial_category = models.ForeignKey(TutorialCategory, default=1, verbose_name="Category", on_delete=models.SET_DEFAULT)
    #parent = forms.ForeignKey(parents,default=1,verbose_name="parent",on_delete=models.SET_DEFAULT)
    firstname = forms.CharField(max_length=100)
    middlename = forms.CharField(max_length=100)
    lastname = forms.CharField(max_length=100)
    classlevel = forms.ChoiceField(choices=classlevel_choices)
    age = forms.ChoiceField(choices=age_choices)
    gender = forms.ChoiceField(choices=gender_choices)
    time_of_study = forms.ChoiceField(choices=time_of_study_choices)
    campus = forms.ChoiceField(choices=campus_choices)

    class Meta:
        model = children
        exclude = ('parent',)

bank_choices = (("PostBank", "PostBank"),)
term_choices = ( ("1", "One"), ("2", "Two"), ("3", "Three"),)
year_choices = ( ("2021", "2021"), ("2022", "2022"),("2023","2023"),("2024","2024"))
installment_choices = ( ("1", "One"), ("2", "Two"), ("3", "Three"),("4","Four"),)
class payments_form(forms.ModelForm):
    child = forms.IntegerField(label="Childid")
    amount = forms.IntegerField()
    YEARS= [x for x in range(2020,2025)]
    date = forms.DateField(label='Date', widget=forms.SelectDateWidget(years=YEARS),initial=timezone.now())
    #birth_date= forms.DateField(label='What is your birth date?', widget=forms.SelectDateWidget(years=YEARS))
    bank = forms.ChoiceField(choices=bank_choices)
    term = forms.ChoiceField(choices=term_choices)
    # year = forms.ChoiceField(choices=year_choices)
    # installment = forms.ChoiceField(choices=installment_choices)

    class Meta:
        model = payments
        exclude = ('id',)

requirements_installment_choices = ( ("1", "One"), ("2", "Two"), ("3", "Three"),)
requirements_bag_choices = ( ("0", "Zero"),("1", "One"), ("2", "Two"),)
requirements_egg_choices = ( ("0", "Zero"),("1", "One"), ("2", "Two"),)
class requirements_form(forms.ModelForm):
    YEARS= [x for x in range(2020,2025)]

    child = forms.IntegerField(label="Childid")
    amount = forms.IntegerField()
    date = forms.DateField(label='Date', widget=forms.SelectDateWidget(years=YEARS),initial=timezone.now())
    # installment = forms.ChoiceField(choices = requirements_installment_choices)
    bag = forms.ChoiceField(choices = requirements_bag_choices)
    egg = forms.ChoiceField(choices = requirements_egg_choices)
    
    class Meta:
        model = requirements
        exclude = ('id',)

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ('username', 'email', 'password')
